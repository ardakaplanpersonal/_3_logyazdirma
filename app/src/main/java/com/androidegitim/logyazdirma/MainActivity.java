package com.androidegitim.logyazdirma;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("BU İLK PARAMETRE", "BU İKİNCİ PARAMETRE");
        Log.d("BU İLK PARAMETRE", "BU İKİNCİ PARAMETRE");
        Log.e("BU İLK PARAMETRE", "BU İKİNCİ PARAMETRE");
        Log.w("BU İLK PARAMETRE", "BU İKİNCİ PARAMETRE");
        Log.v("BU İLK PARAMETRE", "BU İKİNCİ PARAMETRE");
        Log.wtf("BU İLK PARAMETRE", "BU İKİNCİ PARAMETRE");

        findViewById(R.id.main_textview).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.i("UYGULAMA TAGI", "TEXTVİEW A BASILDI");
            }
        });
    }
}
